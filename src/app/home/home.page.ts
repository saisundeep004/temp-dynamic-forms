import { Component              }   from    '@angular/core';
import { FormGroup              }   from    '@angular/forms';
import {
    DynamicFormModel,
    DynamicCheckboxModel,
    DynamicInputModel,
    DynamicRadioGroupModel,
    DynamicFormService 
}   from    "@ng-dynamic-forms/core";

export const MY_FORM_MODEL: DynamicFormModel = [

    new DynamicInputModel({

        id: "sampleInput",
        label: "Sample Input",
        maxLength: 42,
        placeholder: "Sample input"
    }),

    new DynamicRadioGroupModel<string>({

        id: "sampleRadioGroup",
        label: "Sample Radio Group",
        options: [
            {
                label: "Option 1",
                value: "option-1",
            },
            {
                label: "Option 2",
                value: "option-2"
            },
            {
                label: "Option 3",
                value: "option-3"
            }
        ],
        value: "option-3"
    }),

    new DynamicCheckboxModel({

        id: "sampleCheckbox",
        label: "I do agree"
    })
];

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
    public formModel: DynamicFormModel = MY_FORM_MODEL;
    public formGroup: FormGroup;
    constructor(
        private formService: DynamicFormService
    ) {}

    ngOnInit() {
        this.formGroup          =   this.formService.createFormGroup(this.formModel);
        this.formGroup.valueChanges.subscribe(res => {
            console.log(res);
        });
    }

}
