import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HomePage } from './home.page';

import { DynamicFormsCoreModule } from "@ng-dynamic-forms/core";
import { DynamicFormsMaterialUIModule } from "@ng-dynamic-forms/ui-material";
import { DynamicFormsIonicUIModule } from "@ng-dynamic-forms/ui-ionic";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DynamicFormsCoreModule,
    DynamicFormsMaterialUIModule,
    DynamicFormsIonicUIModule,
   
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
